import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {FooterComponent} from "./component/footer/footer.component";
import {ProjectsComponent} from "./pages/projects/projects.component";
import {NavigationComponent} from "./component/navigation/navigation.component";
import {HeaderComponent} from "./component/header/header.component";
import {HomeComponent} from "./pages/home/home.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, FooterComponent, ProjectsComponent, NavigationComponent, HeaderComponent, HomeComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'phippy';
}
